#![cfg_attr(
  all(not(debug_assertions), target_os = "windows"),
  windows_subsystem = "windows"
)]
use serde::{Deserialize, Serialize};
use serde_json::Result;


#[derive(Serialize, Deserialize)]
struct MultipleChoiceQuestion<'a> {
  question: &'a str,
  question_type: &'a str,
  category: &'a str,
  answer_a: &'a str,
  answer_b: &'a str,
  answer_c: &'a str,
  answer_d: &'a str,
  correct: &'a str,
  answers_are_numbers: bool,
  answers_are_integers: bool
}

#[tauri::command]
fn add_multiple_choice_question(question: MultipleChoiceQuestion) {
  println!("question is {}", question.question);
}

fn main() {
  tauri::Builder::default()
    .invoke_handler(tauri::generate_handler![add_multiple_choice_question])
    .run(tauri::generate_context!())
    .expect("error while running tauri application");
}

