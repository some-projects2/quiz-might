import styles from "./Home.module.css";

const Home = () => (
  <div class={styles.home}>
    <h1>Quiz Might</h1>
    <a href="/start">Start</a>
    <a href="/add-questions">Add Questions</a>
    <a href="/quit">Quit</a>
  </div>
);

export default Home;
