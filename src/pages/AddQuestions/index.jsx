import { invoke } from "@tauri-apps/api";

import { questionTypes } from "../../constants";
import styles from "./AddQuestions.module.css";

const AddQuestions = () => {
  let answerA, answerB, answerC, answerD;

  let handleCategorySelect = (e) => (category = e.target.value);

  // multiple choice data structure
  // {question: '', type: 'multiple-choice', category: '', answerA: '', answerB: '', answerC: '', answerD: '', correct: 'answerA'}

  // open-answer
  // {question: '', type: 'open-answer', category: '', answer: ''}
  let handleAddQuestion = () => {
    let question;
    let answers = [answerA, answerB, answerC, answerD];
    let answersAreNumbers = answers.every((a) => typeof a === "number");
    let answersAreIntegers = false;
    if (answersAreNumbers) {
      answersAreIntegers = answers.every(Number.isInteger);
    }
    question = {
      question: "How many tentacles do squid have?",
      questionType: "multiple-choice",
      category: "biology",
      answerA: 1,
      answerB: 2,
      answerC: 8,
      answerD: 10,
      correct: "answerB",
      answersAreNumbers,
      answersAreIntegers,
    };
    console.log("Add Question", JSON.stringify(question));
    invoke("add_multiple_choice_question", question).then(console.log);
  };

  return (
    <main>
      <h1>Quiz Might</h1>
      <div class={styles["add-questions"]}>
        <span>Question</span>
        <input type="text" value="How many tentacles do squid have?" />
        <span>Question Type</span>
        <select name="question-type" id="">
          <option value="multiple-choice">Multiple choice</option>
        </select>
        <span>Answer A</span>
        <textarea name="answer-a" cols="60">
          {answerA}
        </textarea>
        <span>Answer B</span>
        <textarea name="answer-b" cols="60">
          {/* {answerB} */}2
        </textarea>
        <span>Answer C</span>
        <textarea name="answer-c" cols="60">
          {/* {answerC} */}8
        </textarea>
        <span>Answer D</span>
        <textarea name="answer-d" cols="60">
          {/* {answerD} */}10
        </textarea>
        <span>Answer</span>
        <input type="text" value="2" />
        <span>Category</span>
        <select onChange={(e) => console.log("e", e)}>
          {/* {#each Object.keys($categories) as category}
            <option value={category}>{category}</option>
            {/each} */}
          <option value="biology">Biology</option>
          <option value="chemistry">Chemistry</option>
        </select>
        <span>Add Category</span>
        {/* <input type="text" on:keypress={handleAddCategory} bind:value={addCategoryInput} /> */}
        <button type="button" onClick={handleAddQuestion}>
          Add
        </button>
        {/* <button type="button" on:click={() => console.log('questionsAndAnswers', $questionsAndAnswers)}>Log</button> */}
        {/* <button type="button" on:click={handleSave}>Save</button> */}
        <a href="/">Quit</a>
        <button onClick={() => console.log("answerA", answerA)}>Log</button>
      </div>
      <hr />
      <div class={styles["questions-and-answers"]}>
        {/* {#each Object.entries($questionsAndAnswers) as [id, qAndA]}
            <div class="question">
                <span on:click={handleRemoveQuestion(id)}>[x]</span>
                <span>{qAndA.question}</span>
                <span class="category">{qAndA.category}</span>
                {#if (qAndA.type == 'multiple-choice') }
                    <p>{qAndA.answerA}</p>
                    <p>{qAndA.answerB}</p>
                    <p>{qAndA.answerC}</p>
                    <p>{qAndA.answerD}</p>
                {:else if (qAndA.type == 'open-answer') }
                    <p>{qAndA.answer}</p>
                {/if}
            </div>
        {/each} */}
      </div>
    </main>
  );
};

export default AddQuestions;
