import { Routes, Route } from "solid-app-router";

import Home from "./pages/Home";
import AddQuestions from "./pages/AddQuestions";

import styles from "./App.module.css";

function App() {
  return (
    <div class={styles.App}>
      <Routes>
        <Route path="/add-questions" element={<AddQuestions />} />
        <Route path="/" element={<Home />} />
      </Routes>
    </div>
  );
}

export default App;
